<?php

namespace App\Providers;

use App\Services\DeliveryServiceInterface;
use App\Services\NovaposhtaService;
use Illuminate\Support\ServiceProvider;
use Exception;

class DeliveryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DeliveryServiceInterface::class, function ($app) {
            $courierType = request()->input('courier_type');

            // Debug line
            var_dump($courierType);

            switch ($courierType) {
                case 'novaposhta':
                    return new NovaposhtaService();
                case 'ukrposhta':
                    return new UkrposhtaService();
                default:
                    throw new Exception('Invalid courier type');
            }
        });
    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
