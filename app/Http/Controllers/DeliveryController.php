<?php

namespace App\Http\Controllers;

use App\Services\DeliveryServiceInterface;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    protected DeliveryServiceInterface $deliveryService;

    public function __construct(DeliveryServiceInterface $deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }

    public function send(Request $request)
    {
        $parcelData = $request->input('parcel');
        $recipientData = $request->input('recipient');

        $mail = $this->deliveryService->sendParcel($parcelData, $recipientData);

        return response()->json(['message' => 'Parcel sent successfully', 'mail' => $mail]);
    }
}
