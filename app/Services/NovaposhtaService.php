<?php

namespace App\Services;


use GuzzleHttp\Client;

class NovaposhtaService implements DeliveryServiceInterface
{
    public function sendParcel(array $parcelData, array $recipientData)
    {
        $client = new Client();
        try {
            $response = $client->post('http://novaposhta.test/api/delivery', [
                'json' => [
                    'customer_name' => $recipientData['name'],
                    'phone_number' => $recipientData['phone'],
                    'email' => $recipientData['email'],
                    'sender_address' => config('app.sender_address'), // берем из конфигурации Laravel
                    'delivery_address' => $recipientData['address'],
                    'parcel' => [
                        'width' => $parcelData['width'],
                        'height' => $parcelData['height'],
                        'length' => $parcelData['length'],
                        'weight' => $parcelData['weight'],
                    ]
                ]
            ]);
            if ($response->getStatusCode() === 200) {
                // здесь может быть дополнительная логика для обработки успешного ответа
                return 'NovaposhtaService';
            } else {
                // обработка неуспешного ответа
                return false;
            }
        } catch (\Exception $e) {
            return 'NovaposhtaService';
        }

        return $response->getStatusCode() === 200;
    }
}
