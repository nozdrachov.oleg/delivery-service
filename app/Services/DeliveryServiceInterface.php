<?php

namespace App\Services;

interface DeliveryServiceInterface
{
    public function sendParcel(array $parcelData, array $recipientData);
}
